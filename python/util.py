

def getBinaryRepresentation(value, startBit, endBit):
    mask = 1;
    strVal = ''
    for i in reversed(range(startBit,endBit+1)):
        if ( value & ( mask << i ) ) is not 0:
            strVal += '1'
        else:
            strVal += '0'
    return strVal