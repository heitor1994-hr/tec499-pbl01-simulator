import memory
import registersFile
import operations

currentInstruction = 0

def instructionFetch():
    global currentInstruction
    instructionSize = memory.processorInstructionSize
    address = currentInstruction
    instruction = memory.read(address, instructionSize)
    currentInstruction += instructionSize
    return instruction

def executeInstruction(instruction):
    operations.execute(instruction)

def executeProcessor():
    global currentInstruction
    while currentInstruction < 5000:
        instruction = instructionFetch()
        print currentInstruction/4,currentInstruction
        if int(instruction,2) == 0:
            break;
        executeInstruction(instruction)

def setPC(address):
    global currentInstruction
    pc4Msb = currentInstruction & 0xf0000000
    address = address & 0x0fffffff
    currentInstruction = pc4Msb | address
    

def addPC(val):
    global currentInstruction
    currentInstruction += val

def getPC():
    global currentInstruction
    return currentInstruction

def setup():
    memory.clear()
    registersFile.clear()
    registersFile.write(29, memory.memorySize-1)
    registersFile.write(30, memory.memorySize-1)
    registersFile.write(28, 32768)