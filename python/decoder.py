

class Decoder(object):

    def __init__ (self, instructionBinary):
        self.instructionBinary = instructionBinary
    
    def opcode(self):
        return int(self.instructionBinary[:6],2)

class Rdecoder(Decoder):

    def __init__ (self, instructionBinary):
        Decoder.__init__(self,instructionBinary)
    
    def rs(self):
        return int(self.instructionBinary[6:11],2)
    
    def rt(self):
        return int(self.instructionBinary[11:16],2)

    def rd(self):
        return int(self.instructionBinary[16:21],2)
    
    def shift(self):
        return int(self.instructionBinary[21:26],2)
    
    def funct(self):
        return int(self.instructionBinary[26:32])

class Idecoder (Decoder):

    def __init__ (self, instructionBinary):
        Decoder.__init__(self,instructionBinary)

    def rs(self):
        return int(self.instructionBinary[6:11],2)
    
    def rt(self):
        return int(self.instructionBinary[11:16],2)

    def immediate(self):
        return int(self.instructionBinary[16:32],2)

class Jdecoder (Decoder):

    def __init__ (self, instructionBinary):
        Decoder.__init__(self,instructionBinary)

    def address(self):
        return int(self.instructionBinary[6:32],2)

