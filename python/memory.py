from util import getBinaryRepresentation

memorySize = 65536  # memory

ram = [0] * memorySize 

memWordSize = 8

processorInstructionSize = 4 # 4 bytes per instruction

bigEndian = True

def read (address, qt_words):
    retrievedValue = ''
    
    for addr in range(address,address+qt_words):
        if bigEndian:
            retrievedValue += getBinaryRepresentation(ram[addr],0,memWordSize-1)
        else:
            retrievedValue = getBinaryRepresentation(ram[addr],0,memWordSize-1) + retrievedValue
    
    return retrievedValue

def write (address, qt_words, value):

    msb_bit_index = memWordSize*qt_words

    words = []

    while msb_bit_index > 0:
        val = int(getBinaryRepresentation(value,msb_bit_index-memWordSize,msb_bit_index-1),2)
        words.append(val)
        msb_bit_index -= memWordSize

    if not bigEndian:
        words.reverse()

    idx = 0
    for addr in range(address,address+qt_words):
        ram[addr] = int(getBinaryRepresentation(words[idx],0, memWordSize-1), 2)
        idx += 1

def clear():
    for i in range(0,memorySize):
        ram[i] = 0

def printRam (addressBase='dec', valueBase='dec'):
    zeroGroup = False
    startZeroGroupAddr = 0
    endZeroGroupAddr = 0

    for idx, val in enumerate(ram):
        if val == 0 and not zeroGroup:
            startZeroGroupAddr = idx
            zeroGroup = True
        
        if (val != 0 or idx == memorySize-1)  and zeroGroup:
            if idx == memorySize-1:
                endZeroGroupAddr = idx
            else:
                endZeroGroupAddr = idx-1
            printAddresses(startZeroGroupAddr, endZeroGroupAddr,addressBase,valueBase)
            zeroGroup = False
        
        if val != 0:
            printAddresses(idx,idx, addressBase, valueBase)

def printAddresses (startAddr, endAddr, addressBase, valueBase):

    val = ram[startAddr]

    if valueBase == 'hex':
        val = hex(ram[startAddr])
    elif valueBase == 'bin':
        val = bin(ram[startAddr])
    else:
        pass

    if addressBase == 'hex':
        startAddr = hex(startAddr)
        endAddr = hex(endAddr)
    elif addressBase == 'bin':
        startAddr = bin(startAddr)
        endAddr = bin(endAddr)
    else:
        pass
    
    if startAddr != endAddr:
        print '[ ', startAddr, ' ] ... [ ', endAddr, ' ]: ', val
    else:
        print '[ ', startAddr, ' ]: ', val



def loadMemoryFromFile(filename):
    currentAddress = 0

    with open(filename,'r') as f:
        for line in f:
            instructionAsDecimal = int(line,2)
            write(currentAddress, processorInstructionSize, instructionAsDecimal)
            currentAddress += processorInstructionSize
            


