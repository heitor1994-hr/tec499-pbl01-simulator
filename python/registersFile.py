from util import getBinaryRepresentation

fileSize = 32 # 32 GPR registers
registerSize = 4 # 4 bytes per register

accumulator = 0 # acumulador

registers = [0] * fileSize # registers file     


def read (reg):

    if isinstance(reg,basestring):
        reg = int(reg,2)
    
    return registers[reg]

def write(reg, value):

    value = value & 0xffffffff

    if isinstance(reg,basestring):
        reg = int(reg,2)
    
    registers[reg] = value


def readHi ():
    return int(getBinaryRepresentation(accumulator,16, 31),2)

def readLi():
    return int(getBinaryRepresentation(accumulator,0, 15),2)

def sumAcc(val):
    accumulator += val

def writeHi(val):
    global accumulator
    val = val << 16
    accumulator = accumulator & 0x0000ffff
    accumulator = accumulator | val

def writeLo(val):
    global accumulator
    accumulator = accumulator & 0xffff0000
    accumulator = accumulator | val

def clear():
    global accumulator
    accumulator = 0
    for i in range(0,fileSize):
        registers[i] = 0

def printRegisters():

    print 'Hi: ', readHi()
    print 'Li: ', readLi()

    for idx,val in enumerate(registers):
        print 'R',idx,'(',getRegisterName(idx),'): ',val
    

def getRegisterName(idx):

    if idx == 0:
        return 'zero'
    elif idx == 1:
        return 'at'
    elif idx == 2 or idx == 3:
        return 'v' + str(idx-2)
    elif idx >= 4 and idx <= 7:
        return 'a' + str(idx-4)
    elif idx >= 8 and idx <= 15:
        return 't' + str(idx-8)
    elif idx >= 16 and idx <= 23:
        return 's' + str(idx-16)
    elif idx >= 24 and idx <= 25:
        return 't' + str(idx-24)
    elif idx >= 26 and idx <= 27:
        return 'k' + str(idx-26)
    elif idx == 28:
        return 'gp'
    elif idx == 29:
        return 'sp'
    elif idx == 30:
        return 's8/fp'
    elif idx == 31:
        return 'ra'
