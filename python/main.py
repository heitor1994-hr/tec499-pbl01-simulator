from __future__ import print_function
import sys
import memory
import registersFile
import decoder
import controller



def main(filename):
    
    controller.setup()

    memory.loadMemoryFromFile(filename)

    controller.executeProcessor()

    memory.printRam()
    registersFile.printRegisters()


if __name__ == "__main__":
    main(sys.argv[1])
