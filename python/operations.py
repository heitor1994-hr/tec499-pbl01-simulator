import memory
import decoder
import registersFile
import controller

Ropcodes = [ 0, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17,
            18, 20, 21, 23, 25, 26, 28, 30, 31, 32, 33, 34, 35,
            37, 38, 39, 40, 41, 42, 43, 49, 51, 58, 59, 60, 63 ]

Iopcodes = [ 1, 2, 10, 19, 22, 24, 27, 29, 36, 44, 45, 46, 47, 52, 53, 54, 55, 56, 57, 61, 62]

Jopcodes = [ 48, 50]


def isRopcode(opcode):
    if opcode in Ropcodes:
        return True
    else:
        return False
    
def isIopcode(opcode):
    if opcode in Iopcodes:
        return True
    else:
        return False

def isJopcode(opcode):
    if opcode in Jopcodes:
        return True
    else:
        return False

def immediateSignExtend(immediate):

    mask = 1 << 15

    if immediate & mask:
        immediate = ~immediate
        immediate = immediate & 0x00ff
        immediate += 1
        immediate = -immediate

    return immediate        


def getDecoder(instruction):
    d = decoder.Decoder(instruction)

    if isRopcode(d.opcode()):
        return decoder.Rdecoder(instruction)
    elif isIopcode(d.opcode()):
        return decoder.Idecoder(instruction)
    elif isJopcode(d.opcode()):
        return decoder.Jdecoder(instruction) 


def add (rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal + rtVal

    registersFile.write(rd, res)

def addi (rt, rs, immediate):
    rsVal = registersFile.read(rs)

    immediate = immediateSignExtend(immediate)

    res = rsVal + immediate

    registersFile.write(rt, res)

def addiu (rt, rs, immediate):
    rsVal = registersFile.read(rs)

    immediate = immediateSignExtend(immediate)

    res = rsVal + immediate

    registersFile.write(rt, res)

def addu (rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal + rtVal

    registersFile.write(rd, res)

def clo (rd, rs):
    rsVal = registersFile.read(rs)

    count = 0

    mask = 1
    for i in reversed(range(0,31)):
        if rsVal & (mask << i):
            count += 1
        else:
            break
    
    registersFile.write(rd, count)

def clz (rd, rs):
    rsVal = registersFile.read(rs)

    count = 0

    mask = 1
    for i in reversed(range(0,31)):
        if rsVal & (mask << i):
            break
        else:
            count += 1
    
    registersFile.write(rd, count)

def sub (rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal - rtVal

    registersFile.write(rd, res)

def subu (rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal - rtVal

    registersFile.write(rd, res)

def seb (rd, rs):
    rsVal = registersFile.read(rs)

    res = 0

    mask = 1 << 7

    if mask & rsVal:
        res = rsVal | 0xffffff00
    else:
        res = rsVal & 0x000000ff

    registersFile.write(rd, res)

def seh (rd, rs):
    rsVal = registersFile.read(rs)

    res = 0

    mask = 1 << 15

    if mask & rsVal:
        res = rsVal | 0xffff0000
    else:
        res = rsVal & 0x0000ffff

    registersFile.write(rd, res)

def lui (rt, immediate):    
    res = immediate << 16

    registersFile.write(rt, res)

def neg (rd, rs):
    rsVal = registersFile.read(rs)

    res = -rsVal

    registersFile.write(rd, res)

def sll(rd, rs, shift):
    rsVal = registersFile.read(rs)

    res = rsVal << shift

    registersFile.write(rd, res)

def sllv(rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal << rtVal

    registersFile.write(rd, res)

def sra(rd,rs,shift):
    rsVal = registersFile.read(rs)

    res = rsVal >> shift

    registersFile.write(rd, res)

def srav(rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal >> rtVal

    registersFile.write(rd, res)

def srl (rd, rs, shift):
    rsVal = registersFile.read(rs)

    res = rsVal >> shift if rsVal >= 0 else (rsVal + 0x100000000)>>shift

    registersFile.write(rd, res)

def srlv (rd, rs, rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal >> rtVal if rsVal >= 0 else (rsVal + 0x100000000)>>rtVal

    registersFile.write(rd, res)

def _and(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal & rtVal

    registersFile.write(rd, res)

def _andi(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    res = rsVal & immediate

    registersFile.write(rt, res)

def _nor(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = ~(rsVal | rtVal)

    registersFile.write(rd, res)

def _or(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal | rtVal

    registersFile.write(rd, res)

def _ori(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    res = rsVal | immediate

    registersFile.write(rt, res)

def _xor(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal ^ rtVal

    registersFile.write(rd, res)

def _xori(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    res = rsVal ^ immediate

    registersFile.write(rt, res)

def _xor(rd,rs):
    rsVal = registersFile.read(rs)

    res = ~rsVal

    registersFile.write(rd, res)

def _nand(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = ~(rsVal & rtVal)

    registersFile.write(rd, res)

def _nand(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    res = ~(rsVal & immediate)

    registersFile.write(rt, res)

def div(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    if rtVal != 0: 
        quo = int(rsVal/rtVal)
        rest = rsVal%rtVal
    else:
        quo = 0
        rest = 0
    

    registersFile.writeHi(rest)
    registersFile.writeLo(quo)

def divi(rs,immediate):
    rsVal = registersFile.read(rs)

    quo = int(rsVal/immediate)
    rest = rsVal%immediate

    registersFile.writeHi(rest)
    registersFile.writeLo(quo)


def divu(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    quo = int(rsVal/rtVal)
    rest = rsVal%rtVal

    registersFile.writeHi(rest)
    registersFile.writeLo(quo)

def madd(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal

    registersFile.sumAcc(res)


def maddu(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal

    registersFile.sumAcc(res)

def msub (rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal
    res = -res

    registersFile.sumAcc(res)


def msubu (rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal
    res = -res

    registersFile.sumAcc(res)

def mul(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal

    registersFile.write(rd, res)

def muli(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    res = rsVal * immediate

    registersFile.write(rt, res)

def mulu(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal

    registersFile.write(rd, res)

def mult(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal
    hi = (res & 0xffff0000)>>16
    lo = (res & 0x0000ffff)

    registersFile.writeHi(hi)
    registersFile.writeLo(lo)

def multu(rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    res = rsVal * rtVal
    hi = (res & 0xffff0000)>>16
    lo = (res & 0x0000ffff)

    registersFile.writeHi(hi)
    registersFile.writeLo(lo)

def mfhi(rd):
    hi = registersFile.readHi()

    registersFile.write(rd, hi)

def mflo(rd):
    lo = registersFile.readLo()

    registerFile.write(rd, lo)

def mthi(rs):
    rsVal = registersFile.read(rs)

    registersFile.writeHi(rsVal)

def mtlo(rs):
    rsVal = registersFile.read(rs)

    registerFile.writeLo(rsVal)

def bgez(rs,immediate):
    rsVal = registersFile.read(rs)

    immediate = immediateSignExtend(immediate)    

    immediate = immediate * memory.processorInstructionSize

    if rsVal >= 0:
        controller.addPC(immediate)
    
def bltz(rs,immediate):
    rsVal = registersFile.read(rs)

    immediate = immediateSignExtend(immediate)    

    immediate = immediate * memory.processorInstructionSize

    if rsVal < 0:
        controller.addPC(immediate)

def beq(rt, rs,immediate):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    immediate = immediateSignExtend(immediate)    

    immediate = immediate * memory.processorInstructionSize

    if rsVal == rtVal:
        controller.addPC(immediate)
    

def bne(rt, rs,immediate):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    immediate = immediateSignExtend(immediate)    

    immediate = immediate * memory.processorInstructionSize

    if rsVal != rtVal:
        controller.addPC(immediate)

def j (addr):
    address = addr * memory.processorInstructionSize
    controller.setPC(address)

def jr (rs):
    rsVal = registersFile.read(rs)

    controller.setPC(rsVal)

def jal(addr):
    address = addr * memory.processorInstructionSize
    registersFile.write(31,controller.getPC())

    controller.setPC(address)

def jalr (rd,rs):
    rsVal = registersFile.read(rs)
    registersFile(31,controller.getPC())
    controller.setPC(rsVal)

def lb (rt,immediate,rs):
    rsVal = registersFile.read(rs)

    address = rsVal + immediate

    res = int(memory.read(address, 1),2)

    registersFile.write(rt, res)

def lw (rt,immediate,rs):
    rsVal = registersFile.read(rs)

    address = rsVal + immediate

    res = int(memory.read(address, 4),2)

    registersFile.write(rt, res)

def lh (rt,immediate,rs):
    rsVal = registersFile.read(rs)

    address = rsVal + immediate

    res = int(memory.read(address, 2),2)

    registersFile.write(rt, res)

def sb (rt,immediate,rs):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    address = rsVal + immediate

    memory.write(address,1,rtVal)

def sh (rt,immediate,rs):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    address = rsVal + immediate

    memory.write(address,2,rtVal)

def sw (rt,immediate,rs):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    address = rsVal + immediate
    
    memory.write(address,4,rtVal)

def movn(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    if rtVal != 0:
        registersFile.write(rd,rsVal)

def movz(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    if rtVal == 0:
        registersFile.write(rd,rsVal)

def slt(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    if rsVal < rtVal:
        registersFile.write(rd,1)
    else:
        registersFile.write(rd,0)

def slti(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    if rsVal < immediate:
        registersFile.write(rt,1)
    else:
        registersFile.write(rt,0)

def sltiu(rt,rs,immediate):
    rsVal = registersFile.read(rs)

    if rsVal < immediate:
        registersFile.write(rt,1)
    else:
        registersFile.write(rt,0)

def sltu(rd,rs,rt):
    rsVal = registersFile.read(rs)
    rtVal = registersFile.read(rt)

    if rsVal < rtVal:
        registersFile.write(rd,1)
    else:
        registersFile.write(rd,0)

def execute(instruction):
    d = getDecoder(instruction)

    opcode = d.opcode()


    if opcode == 0: add(d.rd(), d.rs(), d.rt())
    elif opcode == 1: addi (d.rt(), d.rs(), d.immediate())
    elif opcode == 2: addiu (d.rt(), d.rs(), d.immediate())
    elif opcode == 3: addu (d.rd(), d.rs(), d.rt())
    elif opcode == 4: clo (d.rd(), d.rs())
    elif opcode == 5: clz (d.rd(), d.rs())
    elif opcode == 6: sub (d.rd(), d.rs(), d.rt())
    elif opcode == 7: subu (d.rd(), d.rs(), d.rt())
    elif opcode == 8: seb (d.rd(), d.rs())
    elif opcode == 9: seh (d.rd(), d.rs())
    elif opcode == 10: lui (d.rt(), d.immediate())
    elif opcode == 11: neg (d.rt(), d.rs())
    elif opcode == 12: sll (d.rd(), d.rs(), d.shift())
    elif opcode == 13: sllv (d.rd(), d.rs(), d.rt())
    elif opcode == 14: sra (d.rd(), d.rs(), d.shift())
    elif opcode == 15: srav (d.rd(), d.rs(), d.rt())
    elif opcode == 16: srl (d.rd(), d.rs(), d.shift())
    elif opcode == 17: srlv (d.rd(), d.rs(), d.rt())
    elif opcode == 18: _and (d.rd(), d.rs(), d.rt())
    elif opcode == 19: _andi (d.rt(), d.rs(), d.immediate())
    elif opcode == 20: _nor (d.rd(), d.rs(), d.rt())
    elif opcode == 21: _or (d.rd(), d.rs(), d.rt())
    elif opcode == 22: _ori (d.rt(), d.rs(), d.immediate())
    elif opcode == 23: _xor (d.rd(), d.rs(), d.rt())
    elif opcode == 24: _xori (d.rt(), d.rs(), d.immediate())
    elif opcode == 25: _not (d.rd(), d.rs())
    elif opcode == 26: _nand (d.rd(), d.rs(), d.rt())
    elif opcode == 27: _nandi (d.rt(), d.rs(), d.immediate())
    elif opcode == 28: div (d.rs(), d.rd())
    elif opcode == 29: divi (d.rs(), d.immediate())
    elif opcode == 30: divu (d.rs(), d.rt())
    elif opcode == 31: madd (d.rs(), d.rt())
    elif opcode == 32: maddu (d.rs(), d.rt())
    elif opcode == 33: msub (d.rs(), d.rt())
    elif opcode == 34: msubu (d.rs(), d.rt())
    elif opcode == 35: mul (d.rd(),d.rs(), d.rt())
    elif opcode == 36: muli (d.rt(),d.rs(), d.immediate())
    elif opcode == 37: mulu (d.rd(),d.rs(), d.rt())
    elif opcode == 38: mult (d.rs(),d.rt())
    elif opcode == 39: multu (d.rs(),d.rt())
    elif opcode == 40: mfhi (d.rd())
    elif opcode == 41: mflo (d.rd())
    elif opcode == 42: mthi (d.rs())
    elif opcode == 43: mtlo (d.rs())
    elif opcode == 44: bgez (d.rs(), d.immediate())
    elif opcode == 45: bltz (d.rs(), d.immediate())
    elif opcode == 46: beq (d.rt(), d.rs(), d.immediate())
    elif opcode == 47: bne (d.rt(), d.rs(), d.immediate())
    elif opcode == 48: j (d.address())
    elif opcode == 49: jr (d.rs())
    elif opcode == 50: jal (d.address())
    elif opcode == 51: jalr (d.rd(), d.rs())
    elif opcode == 52: lb (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 53: lw (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 54: sb (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 55: sw (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 56: lh (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 57: sh (d.rt(),d.immediate() ,d.rs() )
    elif opcode == 58: movn (d.rd(),d.rs() ,d.rt() )
    elif opcode == 59: movz (d.rd(),d.rs() ,d.rt() )
    elif opcode == 60: slt (d.rd(),d.rs() ,d.rt() )
    elif opcode == 61: slti (d.rt(),d.rs() ,d.immediate() )
    elif opcode == 62: sltiu (d.rt(),d.rs() ,d.immediate() )
    elif opcode == 63: sltu (d.rd(),d.rs() ,d.rt() )
