#ifndef FETCHER
#define FETCHER

int read_pc();
void clear_pc();
void update_pc(int new_addr);
void increment_pc();
int fetch_instruction();

#endif
