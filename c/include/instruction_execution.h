#ifndef INST_EXECUTION
#define INST_EXECUTION

void execute_instruction(int args[]);

void initialize_executor();


// Instructions Functions
void add(int[]);
void sub(int[]);
void addi(int[]);
void addiu(int[]);
void addu(int[]);
void clo(int[]);
void clz(int[]);
void lui(int[]);
void se(int[]);
void seb(int[]);
void seh(int[]);
void sub(int[]);
void subu(int[]);
void sll(int[]);
void sllv(int[]);
void sra(int[]);
void srav(int[]);
void srl(int[]);
void srlv(int[]);
void and(int[]);
void andi(int[]);
void nor(int[]);
void or(int[]);
void ori(int[]);
void wsbh(int[]);
void xor(int[]);
void xori(int[]);
void movn(int[]);
void movz(int[]);
void slt(int[]);
void slti(int[]);
void sltiu(int[]);
void sltu(int[]);
void _div(int[]);
void divu(int[]);
void madd(int[]);
void maddu(int[]);
void msub(int[]);
void msubu(int[]);
void mul(int[]);
void mult(int[]);
void multu(int[]);
void mfhi(int[]);
void mflo(int[]);
void mthi(int[]);
void mtlo(int[]);
void beq(int[]);
void b_switch(int[]);
void bgez(int[]);
void bltz(int[]);
void bne(int[]);
void j(int[]);
void jal(int[]);
void jalr(int[]);
void jr(int[]);
void lb(int[]);
void lh(int[]);
void lw(int[]);
void sb(int[]);
void sh(int[]);
void sw(int[]);
#endif