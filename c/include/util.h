
#ifndef UTIL
#define UTIL
// Given a interval, read bits from val
// ex: 20(dec) = 00010100(bin)
// readBitsFromInt(20, 0, 3) = 0100(bin) = 4(dec) 
int readBitsFromInt(long int val, int startBit, int endBit);

// Extend the sign of a value
// ex: 11111111(bin) = -1(dec), but in a 32 bit integer it becomes:
// 00000000000000000000000011111111(bin) = 255 (dec)
// so, to keep the same value in a 32 bit register, it must become:
// 11111111111111111111111111111111(bin) = -1(dec)
// in this case, it can be done calling signExtend(255, 7, 32)
int signExtend(int val, int signBitPos, int targetSize);

// Print val as a 64 bit binary string
void printBits(long int val);

int getBit(int val, int pos);

int inArray(int val, int arr[], int length);

int writeBitsToInt(long int val, int endBit);

#endif
