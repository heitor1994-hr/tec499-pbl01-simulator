#ifndef REGISTERS_FILE
#define REGISTERS_FILE

// Read the content of a register from the registers file
int readRegister(int reg);

// Write a value in a given register
void writeOnRegister(int reg, int val);

// Read bits [32..63] from accumulator 
int readHi();

// Read bits [0..31] from accumulator
int readLo();

// Write a value over the accumulator bits [32..63] 
void writeOnHi(int val);

// Write a value over the accumulator bits [0..31]
void writeOnLo(int val);

//Set GPRs and accumulator to 0
void clearRegisters();

// Print registers file out stdout
void printRegisters();

long int readAccumulator();

void preAllocateRegisters();

void printRegistersBinary();

#endif