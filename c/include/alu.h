#ifndef ALU
#define ALU


int addition(int a, int b);
int addition_cheking_overflow(int a, int b);

int subtraction(int a, int b);
int subtraction_checking_overflow(int a, int b);

long int multiplication(int a, int b);
long int multiplication_unsigned(unsigned int a, unsigned int b);
int* division(int a, int b, int accumulator[]);
int* division_unsigned(unsigned int a, unsigned int b, int accumulator[]);

int and_operation(int a, int b);
int or_operation(int a, int b);
int xor_operation(int a, int b);
int not_operation(int a);

void check_flags(long int temp);
int check_overflow_flag(long int temp);
int check_carry_flag(long int temp);
int check_negative_flag(long int temp);
int check_zero_flag(long int temp);

#endif
