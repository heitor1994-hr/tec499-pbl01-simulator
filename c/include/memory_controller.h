#ifndef MEM_CONTROLLER
#define MEM_CONTROLLER



void fill_memory(const char *filepath);
int load_byte(int address);
void store_byte(int address, int buffer);
int load_half_word(int address);
void store_half_word(int address, int buffer);
int load_word(int address);
void store_word(int address, int buffer);
void clear_memory();
void allocate_memory();
int first_text_seg_addr();
int last_text_seg_addr();
int first_data_seg_addr();
int last_data_seg_addr();
void print_memory(FILE* out, int hexText, int hexData, int hexStack);
void print_text_segment(FILE* out, int hexText);
void print_data_segment(FILE* out, int hexData);
void print_stack_segment(FILE* out, int hexStack);
void print_binary (FILE *out);
#endif
