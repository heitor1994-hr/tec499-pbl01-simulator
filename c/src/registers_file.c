#include <registers_file.h>
#include <util.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_SIZE 32
#define STACK_POINTER 0x10000;


static int GPR[FILE_SIZE];

static char registersNames[FILE_SIZE][6] = {"zero","at", "v0", "v1","a0","a1","a2","a3","t0"
                                            ,"t1","t2","t3","t4","t5","t6","t7","s0","s1","s2"
                                            ,"s3","s4","s5","s6","s7","t8","t9","k0","k1","gp"
                                            ,"sp","s8/fp","ra"};

static long int accumulator = 0L;

int readRegister(int reg) {
    return GPR[reg];
}

void writeOnRegister (int reg, int val) {
    GPR[reg] = val;
}

int readHi (){
    return ((0xffffffff00000000 & accumulator) >> 32);
}

int readLo() {
    return (0x00000000ffffffff & accumulator);
}

long int readAccumulator(){
    return accumulator;
}


void writeOnHi(int val) {
    long int valL = val;
    accumulator &= 0x00000000ffffffff;
    accumulator |=  (valL << 32);
}

void writeOnLo(int val) {
    accumulator &= 0xffffffff00000000;
    accumulator |= val;
}

void preAllocateRegisters(){
    GPR[29] = STACK_POINTER;
    GPR[30] = STACK_POINTER;
}

void clearRegisters() {
    int i;
    for (i = 0; i < FILE_SIZE; i++) {
        GPR[i] = 0;
    }
    accumulator = 0L;
}

void printRegisters(FILE *out, int hex) {
    int i;

    const char* fmt = hex ? "0x%x" : "%ld";

    for (i = 0; i < FILE_SIZE; i++) {
        fprintf (out,"\n%02d ", i);
        fprintf (out,"(%5s) = ",registersNames[i]);
        fprintf (out, fmt, GPR[i]);
    }

    fprintf (out,"\nhi = ");
    fprintf (out, fmt, readHi());
    fprintf (out,"\nlo = ");
    fprintf (out, fmt, readLo());
    fprintf (out,"\nacc = ");
    fprintf (out, fmt, accumulator);
    fprintf (out,"\n");

}

void printRegistersBinary (FILE *out) {
    int mask = 1;
	for (int i = 0; i <= FILE_SIZE; i++) {
		int val = GPR[i];
		
		for (int j = 31; j >= 0; j--) {
			if (val & (mask<<j)) {
				fprintf(out,"1");
			} else fprintf(out,"0");
		}
		fprintf (out,"\n");
	}
}

