#include <instruction_decoder.h>
#include <stdlib.h>
#include <util.h>

#define OPCODE_LENGTH 6

#define R_OPS 4
#define I_OPS 17
#define J_OPS 2

static int rInstrsOpcodes[] = {0 , 28, 31, 49 };
static int iInstrsOpcodes[] = {8, 9, 15, 12, 13, 14, 1, 4, 5, 
                                32, 35, 40, 43, 33, 41, 10, 11};
static int jInstrsOpcodes[] = {2, 3};


int isR (int opcode){
    return inArray(opcode,rInstrsOpcodes, R_OPS);
}

int isI (int opcode){
    return inArray(opcode,iInstrsOpcodes, I_OPS);
}

int isJ (int opcode){
    return inArray(opcode, jInstrsOpcodes, J_OPS);
}

int* decode_instruction (int instruction) {
    int* res = (int *)malloc(sizeof(int)*6);

    int opcode = readBitsFromInt(instruction, 26, 31);

    if (isR(opcode)) {
        //opcode
        res[0] = opcode;
        //rs
        res[1] = readBitsFromInt(instruction, 21, 25);
        //rt
        res[2] = readBitsFromInt(instruction, 16, 20);
        //rd
        res[3] = readBitsFromInt(instruction, 11, 15);
        //shift
        res[4] = readBitsFromInt(instruction, 6, 10);
        //funct
        res[5] = readBitsFromInt(instruction, 0, 5);
    } else if (isI(opcode)){
        //opcode
        res[0] = opcode;
        //rs
        res[1] = readBitsFromInt(instruction, 21, 25);
        //rt
        res[2] = readBitsFromInt(instruction, 16, 20);
        //immediate
        res[3] = readBitsFromInt(instruction, 0, 15);
        res[4] = res[5] = 0;
    } else if (isJ(opcode)){
        //opcode
        res[0] = opcode;
        //address
        res[1] = readBitsFromInt(instruction, 0, 25);
        res[2] = res[3] = res[4] = res[5] = 0;
    } 

    return res;
} 

