#include <instruction_execution.h>
#include <registers_file.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory_controller.h>
#include <instruction_fetcher.h>
#include <util.h>
#include <alu.h>

#define OPCODE 0
#define RS 1
#define RT 2
#define RD 3
#define SHIFT 4
#define FUNCT 5
#define ADDR 1
#define IMMED 3

#define MAX_OPCODES 64
#define MAX_FUNCTS 64

// Function pointer matrix
static void (*executor[MAX_OPCODES][MAX_FUNCTS])(int[]);

void initialize_executor () {
    executor[0x0][0x20] = add;  executor[0x0][0x21] = addu;
    executor[0x8][0x0] = addi;  executor[0x9][0x0] = addiu;
    executor[0x0][0x11] = clo;  executor[0x0][0x10] = clz;
    executor[0xf][0x0] = lui;   executor[0x1f][0x20] = se;
    executor[0x0][0x22] = sub;  executor[0x0][0x23] = subu;
    executor[0x0][0x0] = sll;   executor[0x0][0x4] = sllv;
    executor[0x0][0x3] = sra;   executor[0x0][0x7] = srav;
    executor[0x0][0x2] = srl;   executor[0x0][0x6] = srlv;
    executor[0x0][0x24] = and;  executor[0xc][0x0] = andi;
    executor[0x0][0x27] = nor;  executor[0x0][0x25] = or;
    executor[0xd][0x0] = ori;   executor[0x1f][0x20] = wsbh;
    executor[0x0][0x26] = xor;  executor[0xe][0x0] = xori;
    executor[0x0][0xb] = movn;  executor[0x0][0xa] = movz;
    executor[0x0][0x2a] = slt;  executor[0xa][0x0] = slti;
    executor[0xb][0x0] = sltiu; executor[0x0][0x2b] = sltu;
    executor[0x0][0x1a] = _div; executor[0x0][0x1b] = divu;
    executor[0x1c][0x0] = madd; executor[0x1c][0x1] = maddu;
    executor[0x1c][0x4] = msub; executor[0x1c][0x5] = msubu;
    executor[0x1c][0x2] = mul;  executor[0x0][0x18] = mult;
    executor[0x0][0x19] = multu; executor[0x0][0x10] = mfhi;
    executor[0x0][0x12] = mflo; executor[0x0][0x11] = mthi;
    executor[0x0][0x13] = mtlo; executor[0x4][0x0] = beq;
    executor[0x1][0x0] = b_switch; executor[0x5][0x0] = bne;
    executor[0x2][0x0] = j;     executor[0x3][0x0] = jal;
    executor[0x0][0x9] = jalr;  executor[0x0][0x8] = jr;
    executor[0x20][0x0] = lb;   executor[0x21][0x0] = lh;
    executor[0x23][0x0] = lw;   executor[0x28][0x0] = sb;
    executor[0x29][0x0] = sh;   executor[0x2b][0x0] = sw;
}

void execute_instruction(int args[]) {
    int opcode = args[OPCODE];
    int funct = args[FUNCT];

    if (executor[opcode][funct] != NULL) {
        executor[opcode][funct](args);
    }
}

void add (int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]),
    res = addition_cheking_overflow(rs, rt);



    writeOnRegister(args[RD], res);
}

void addi (int args[]) {
    int rs = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);
    int res = addition_cheking_overflow(rs, args[IMMED]);
	
    writeOnRegister(args[RT], res);
}

void addiu (int args[]) {
    int rs = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int res = addition(rs, args[IMMED]);
    
    writeOnRegister(args[RT], res);
}

void addu (int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]),
    res = addition(rs, rt);

    writeOnRegister(args[RD], res);
}

void clo(int args[]) {
    int rs = readRegister(args[RS]);

    int i, temp = 32;

    for (i = temp-1; i >= 0; i--) {
        if (readBitsFromInt(rs, i,i) == 0) {
            temp = 31 - i;
            break;
        }
    }

    writeOnRegister(args[RD], temp);
}

void clz(int args[]) {
    int rs = readRegister(args[RS]);

    int i, temp = 32;

    for (i = temp-1; i >= 0; i--) {
        if (readBitsFromInt(rs, i,i) == 1) {
            temp = 31 - i;
            break;
        }
    }

    writeOnRegister(args[RD], temp);
}

void lui(int args[]) {
    writeOnRegister(args[RT], (args[IMMED]<<16));
}

void se(int args[]){
    switch (args[SHIFT]) {
        case 0x10: seb(args); break;
        case 0x18: seh(args); break;
    }
}

void seb(int args[]){
    int rt = readRegister(args[RT]);

    int res = signExtend(rt, 7, 32);

    writeOnRegister(args[RD], res);
}

void seh(int args[]){
    int rt = readRegister(args[RT]);

    int res = signExtend(rt, 15, 32);

    writeOnRegister(args[RD], res);
}

void sub(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]),
    res = subtraction_checking_overflow(rs, rt);

    writeOnRegister(args[RD], res);
}

void subu(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]),
    res = subtraction(rs, rt);

    writeOnRegister(args[RD], res);
}

void sll(int args[]) {
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], (rt << args[SHIFT]));
}

void sllv(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    rs = readBitsFromInt(rs,0,4);

    writeOnRegister(args[RD], (rt << rs));
}

void sra(int args[]) {
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], (rt >> args[SHIFT]));
}

void srav(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    rs = readBitsFromInt(rs,0,4);

    writeOnRegister(args[RD], (rt >> rs));
}

void srl(int args[]) {
    int rt = readRegister(args[RT]);

    unsigned int temp = (unsigned int)rt;

    writeOnRegister(args[RD], (int)(temp >> args[SHIFT]));
}

void srlv(int args[]) {
    int rt = readRegister(args[RT]);
    int rs = readRegister(args[RS]);

    rs = readBitsFromInt(rs,0,4);
    unsigned int temp = (unsigned int)rs;

    writeOnRegister(args[RD], (int)(temp >> args[SHIFT]));
}

void and(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], and_operation(rs,rt));
}

void andi(int args[]) {
    int rs = readRegister(args[RS]);

    writeOnRegister(args[RT], and_operation(rs,args[IMMED]));
}

void nor(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], not_operation(or_operation(rs,rt)));
}

void or(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], or_operation(rs,rt));
}

void ori(int args[]){
    int rs = readRegister(args[RS]);

    writeOnRegister(args[RT], or_operation(rs,args[IMMED]));
}

void wsbh(int args[]){
    int rt = readRegister(args[RT]);

    int res = 0;
    int temp = readBitsFromInt(rt, 16,23);

    res += writeBitsToInt(temp, 24);

    temp = readBitsFromInt(rt, 24,31);

    res += writeBitsToInt(temp, 16);

    temp = readBitsFromInt(rt, 0,7);

    res += writeBitsToInt(temp, 8);

    temp = readBitsFromInt(rt, 8,15);

    res += writeBitsToInt(temp, 0);

    writeOnRegister(args[RD], res);
}

void xor(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    writeOnRegister(args[RD], xor_operation(rs,rt));
}

void xori(int args[]){
    int rs = readRegister(args[RS]);

    writeOnRegister(args[RT], xor_operation(rs,args[IMMED]));
}

void movn (int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);
    int rd = readRegister(args[RD]);

    int res = rt != 0 ? rs : rd;

    writeOnRegister(args[RD], res);
}

void movz (int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);
    int rd = readRegister(args[RD]);

    int res = rt == 0 ? rs : rd;

    writeOnRegister(args[RD], res);
}

void slt(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    int res = rs < rt ? 1 : 0;

    writeOnRegister(args[RD], res);
}

void slti(int args[]){
    int rs = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int res = rs < args[IMMED] ? 1 : 0;

    writeOnRegister(args[RT], res);

}

void sltiu(int args[]){
    int rs = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int res = (unsigned int)rs < (unsigned int)args[IMMED] ? 1 : 0;

    writeOnRegister(args[RT], res);
}

void sltu(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    int res = (unsigned int)rs < (unsigned int)rt ? 1 : 0;

    writeOnRegister(args[RD], res);
}

void _div(int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    int* res = (int *)malloc(sizeof(int)*2);

    res = division(rs,rt, res);

    writeOnHi(res[0]);
    writeOnLo(res[1]);
}

void divu(int args[]) {
    unsigned int rs = (unsigned int)readRegister(args[RS]);
    unsigned int rt = (unsigned int)readRegister(args[RT]);

    int* res = (int *)malloc(sizeof(int)*2);

    res = division(rs, rt, res);

    writeOnHi(res[0]);
    writeOnLo(res[1]);
}

void madd(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    long int acc = readAccumulator();

    long int temp = multiplication(rs,rt);

    acc += temp;

    writeOnHi(readBitsFromInt(acc, 32,63));
    writeOnLo(readBitsFromInt(acc, 0,31));

}

void maddu(int args[]){
    unsigned int rs = (unsigned int)readRegister(args[RS]);
    unsigned int rt = (unsigned int)readRegister(args[RT]);

    long int acc = readAccumulator();

    long unsigned int temp = multiplication_unsigned(rs,rt);

    acc += temp;

    writeOnHi(readBitsFromInt(acc, 32,63));
    writeOnLo(readBitsFromInt(acc, 0,31));

}

void msub (int args[]) {
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    long int acc = readAccumulator();

    long int temp = multiplication(rs,rt);

    acc -= temp;

    writeOnHi(readBitsFromInt(acc, 32,63));
    writeOnLo(readBitsFromInt(acc, 0,31));
}

void msubu(int args[]){
    unsigned int rs = (unsigned int)readRegister(args[RS]);
    unsigned int rt = (unsigned int)readRegister(args[RT]);

    long int acc = readAccumulator();

    long unsigned int temp = multiplication_unsigned(rs,rt);

    acc -= temp;

    writeOnHi(readBitsFromInt(acc, 32,63));
    writeOnLo(readBitsFromInt(acc, 0,31));
}

void mul (int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    long int res = multiplication(rs,rt);

    writeOnRegister(args[RD], readBitsFromInt(res,0,31));
}

void mult (int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    long int res = multiplication(rs,rt);

    writeOnHi(readBitsFromInt(res,32,63));
    writeOnLo(readBitsFromInt(res,0,31));
}

void multu (int args[]){
    unsigned int rs = (unsigned int)readRegister(args[RS]);
    unsigned int rt = (unsigned int)readRegister(args[RT]);

    long unsigned int res = multiplication(rs,rt);

    writeOnHi(readBitsFromInt(res,32,63));
    writeOnLo(readBitsFromInt(res,0,31));
}

void mfhi(int args[]){
    writeOnRegister(args[RD], readHi());
}

void mflo(int args[]){
    writeOnRegister(args[RD], readLo());
}

void mthi(int args[]){
    int rs = readRegister(args[RS]);
    writeOnHi(rs);
}

void mtlo(int args[]){
    int rs = readRegister(args[RS]);
    writeOnLo(rs);
}

void beq (int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    int pc = read_pc();
    args[IMMED] = signExtend(args[IMMED], 15, 32);
    args[IMMED] = args[IMMED] << 2;

    if (rs == rt) update_pc(addition(pc,args[IMMED]));
}

void bne(int args[]){
    int rs = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    int pc = read_pc();
    args[IMMED] = signExtend(args[IMMED], 15, 32);
    args[IMMED] = args[IMMED] << 2;

    if (rs != rt) update_pc(addition(pc,args[IMMED]));
}

void b_switch(int args[]){
    switch (args[RT]){
        case 0x1: bgez(args); break;
        case 0x0: bltz(args); break;
    }
}

void bgez(int args[]){
    int rs = readRegister(args[RS]);

    int pc = read_pc();
    args[IMMED] = signExtend(args[IMMED], 15, 32);
    args[IMMED] = args[IMMED] << 2;

    if (rs >= 0) update_pc(addition(pc,args[IMMED]));
}

void bltz(int args[]){
    int rs = readRegister(args[RS]);

    int pc = read_pc();
    args[IMMED] = signExtend(args[IMMED], 15, 32);
    args[IMMED] = args[IMMED] << 2;

    if (rs < 0) update_pc(addition(pc,args[IMMED]));
}

void j (int args[]){
    int addr = args[ADDR];
    int pc = read_pc();

    int msb4_pc = (readBitsFromInt(pc,28, 31) << 28);

    addr = addr << 2;
    addr |= msb4_pc;
    update_pc(addr);
}

void jal (int args[]){
    int addr = args[ADDR];
    int pc = read_pc();

    writeOnRegister(31, pc);

    int msb4_pc = (readBitsFromInt(pc,28, 31) << 28);

    addr = addr << 2;
    addr |= msb4_pc;

    update_pc(addr);
}

void jalr (int args[]){
    int rs = readRegister(args[RS]);
    int pc = read_pc();

    writeOnRegister(args[RD], pc);
    update_pc(rs);
}

void jr (int args[]){
    int rs = readRegister(args[RS]);

    update_pc(rs);
}

void lb(int args[]){
    int base = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);
    int res = load_byte(addr);
    res = signExtend(res, 7, 32);
    writeOnRegister(args[RT], res);
}

void lh(int args[]){
    int base = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);
    int res = load_half_word(addr);
    res = signExtend(res, 15, 32);
    writeOnRegister(args[RT], res);
}

void lw(int args[]){
    int base = readRegister(args[RS]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);
    int res = load_word(addr);

    writeOnRegister(args[RT], res);
}

void sb(int args[]){
    int base = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);

    store_byte(addr, rt);
}

void sh(int args[]){
    int base = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);

    store_half_word(addr, rt);
}

void sw(int args[]){
    int base = readRegister(args[RS]);
    int rt = readRegister(args[RT]);

    args[IMMED] = signExtend(args[IMMED], 15, 32);

    int addr = addition(base, args[IMMED]);

    store_word(addr, rt);
}