#include <stdio.h>
#include <stdlib.h>
#include <instruction_fetcher.h>
#include <memory_controller.h>

static int program_counter = 0;

void update_pc(int new_addr)
{
	program_counter = new_addr;
}

int read_pc(){
	return program_counter;
}

void clear_pc() {
	program_counter = 0;
}

void increment_pc()
{
	program_counter = program_counter+4;
}

int fetch_instruction()
{	
	return load_word(program_counter);;
}
