#include <util.h>
#include <stdio.h>

int readBitsFromInt (long int val, int startBit, int endBit) {
    long int mask = 0L;
    int i, res = 0;

    for (i = endBit; i >= startBit; i--) {
        mask = (1L << i);
        res = res | ((val & mask) >> startBit);
    }

    return res;
}


int signExtend (int val, int signBitPos, int targetSize) {
    int signBit = (val & (1 << signBitPos));
    signBit = (signBit >> signBitPos);
    int i;

    for (i = targetSize-1; i > signBitPos; i--) {
        val |= (signBit << i);
    }

    return val;
}

void printBits(long int val) {
    int i;
    printf ("val: %ld : ",val);
    for (i = 63; i >= 0; i--) {
        if ((val & (1L<<i))) printf ("1");
        else printf ("0");
    }
    printf ("\n");
}

int inArray (int val, int arr[], int length) {
    int i;
    for (i = 0; i < length; i++) {
        if (arr[i] == val) return 1;
    }
    return 0;
}

int getBit(int val, int pos)
{
	
	int mask = 1 << pos;
	int masked_val = val & mask;
	
	return masked_val >> pos;
}

int writeBitsToInt(long int val, int endBit)
{
	int res = 0;
	
    return res | (val << endBit);
}

