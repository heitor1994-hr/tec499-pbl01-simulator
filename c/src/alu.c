#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <alu.h>
#include <util.h>
#include <registers_file.h>

static int zero_flag = 0;
static int overflow_flag = 0;
static int carry_flag = 0;
static int negative_flag = 0;


int addition(int a, int b)
{
	int res = a + b;
	
	check_flags(res);
	
	return res;
}
int addition_cheking_overflow(int rs, int rt)
{
    long int temp_rs = (rs&(1<<31))<<1;
    temp_rs |= rs;
    
    long int temp_rt = (rt&(1<<31))<<1;
    temp_rt |= rt;
    
    long int temp = temp_rs + temp_rt;
    
	overflow_flag = check_overflow_flag(temp);
    check_flags(temp);
	
	return temp;
}

int subtraction(int a, int b)
{
	
	int res = a - b;
	
	check_flags(res);
	
	return res;
}

int subtraction_checking_overflow(int rs, int rt)
{
	long int temp_rs = (rs&(1<<31))<<1;
    temp_rs |= rs;
    
    long int temp_rt = (rt&(1<<31))<<1;
    temp_rt |= rt;
    
    long int temp = temp_rs - temp_rt;
    
	overflow_flag = check_overflow_flag(temp);
    check_flags(temp);
		
	return temp;
}

long int multiplication(int a, int b)
{
	long int result = a * b;
	
	return result;
}

long int multiplication_unsigned(unsigned int a, unsigned int b)
{
	long unsigned int result = a * b;
	
	return result;
}

int* division(int a, int b, int accumulator[])
{
	long int result = a / b;
	

	// LO accumulator
	accumulator[1] = readBitsFromInt(result, 0, 31);

	// HI accumulator
	accumulator[0] = a % b;
	
	return accumulator;
}

int* division_unsigned(unsigned int a, unsigned int b, int accumulator[])
{
	long unsigned int result = a / b;
	

	// LO accumulator
	accumulator[1] = readBitsFromInt(result, 0, 31);

	// HI accumulator
	accumulator[0] = a % b;
	
	return accumulator;
}

int and_operation(int a, int b)
{
	
	return a & b;
}
int or_operation(int a, int b)
{
	
	return a | b;
}
int xor_operation(int a, int b)
{
	
	return a ^ b;
}
int not_operation(int a)
{
	
	return ~a;
}

//  Check flags functions

void check_flags(long int temp)
{
    negative_flag = check_negative_flag(temp);
	carry_flag = check_carry_flag(temp);
	zero_flag = check_zero_flag(temp);
}

int check_overflow_flag(long int temp)
{
	
	return xor_operation(getBit(temp, 32), getBit(temp, 31));
}

int check_carry_flag(long int temp)
{
	if(getBit(temp, 32) == 1L)
		return 1;
	else
		return 0;
}

int check_negative_flag(long int temp)
{
	if(temp < 0L)
		return 1;
	else
		return 0;
}

int check_zero_flag(long int temp)
{
	if(temp == 0L)
		return 1;
	else
		return 0;
}
