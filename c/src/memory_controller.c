#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory_controller.h>
#include <util.h>

#define MEMORY_SIZE 65536
#define BIN_INSTR_LENGTH 32
#define HEX_INSTR_LENGTH 8
#define BYTE_LENGTH (BIN_INSTR_LENGTH/4)

#define MEM_OFFSET 4

#define START_TEXT_SEG 0
#define END_TEXT_SEG 4095<<2
#define START_DATA_SEG 4096<<2
#define END_DATA_SEG 8191<<2
#define END_MEMORY 16383<<2


static unsigned char *memory = NULL;

void fill_memory(const char *filepath){
	
	FILE *file;
	size_t read_size;

	char *ext = strrchr(filepath, '.');

	// check if it's a hexadecimal file
	int hex = ext[1] == 'h' ? 1 : 0;
	
	int address = 0;

	file = fopen(filepath, "r");
	if(file){
		
		int length, base;

		if (hex) {
			length = HEX_INSTR_LENGTH;
			base = 16;
		} else {
			length = BIN_INSTR_LENGTH;
			base = 2;
		}

		/**
		 * The instruction set has 32 bits length, which is equivalent to a word (4 bytes)
		 * and each memory address should hold a byte (8 bits). Therefore, the INSTR_LENGTH
		 * is divided by 4 to divide a word into 4 bytes. 
		 */

		unsigned char* buffer = (unsigned char *) malloc(sizeof(unsigned char)*length);
		
		while((read_size = fread(buffer, length+1, 1, file) > 0))
		{	

			int buffer_int = strtol(buffer, NULL, base);
			store_word(address, buffer_int);
			address += MEM_OFFSET;
		}
		
		free(buffer);
		fclose(file);
	}
}

void allocate_memory(){
	memory = (unsigned char*)malloc(sizeof(unsigned char)*MEMORY_SIZE);
}

void clear_memory() {
	if (memory != NULL) memset(memory, 0, MEMORY_SIZE);
}

int load_byte(int address)
{
	int res = (int)memory[address];
	
	return res;
}


void store_byte(int address, int buffer)
{
	unsigned char value = (unsigned char)readBitsFromInt(buffer, 0, BYTE_LENGTH-1);
	memory[address] = value;
}

int load_half_word(int address)
{
	int res = 0, count = 0, i;
	for(i = BIN_INSTR_LENGTH; i > (BIN_INSTR_LENGTH)/2; i = i-BYTE_LENGTH)
	{
		res += writeBitsToInt(memory[address+count], i-(BYTE_LENGTH*3));
		count++;
	}
	
	return res;
}

void store_half_word(int address, int buffer)
{
	int i;
	for(i = BIN_INSTR_LENGTH; i > (BIN_INSTR_LENGTH)/2; i = i-BYTE_LENGTH)
	{
		unsigned char value = (unsigned char)readBitsFromInt(buffer, i-(BYTE_LENGTH*3), i-(BYTE_LENGTH*2)-1);
		
		memory[address] = value;
		address++;
	}
}
int load_word(int address)
{
	int res = 0, count = 0, i;
	
	for(i = BIN_INSTR_LENGTH; i > 0; i = i-BYTE_LENGTH)
	{
		res += writeBitsToInt(memory[address+count], i-BYTE_LENGTH);
		count++;
	}
	
	return res;
}

void store_word(int address, int buffer)
{
	int i;
	
	for(i = BIN_INSTR_LENGTH; i > 0; i = i-BYTE_LENGTH)
	{
		unsigned char value = (unsigned char)readBitsFromInt(buffer, i-BYTE_LENGTH, i-1);
		
		//(*memory)[(*address)].addr_content = (char *) malloc(sizeof(char)*BYTE_LENGTH);
		//memcpy((*memory), &value, BYTE_LENGTH*sizeof(char));
		memory[address] = value;
		
		address++;
	}
}

int first_text_seg_addr(){
	return START_TEXT_SEG;
}

int last_text_seg_addr(){
	return END_TEXT_SEG;
}

int first_data_seg_addr(){
	return START_DATA_SEG;
}

int last_data_seg_addr(){
	return END_DATA_SEG;
}

int last_addr(){
	return END_MEMORY;
}

int first_addr() {
	return START_TEXT_SEG;
}

void print_memory(FILE *out, int hexText, int hexData, int hexStack){
	print_text_segment(out,hexText);
	print_data_segment(out,hexData);
	print_stack_segment(out,hexStack);
}

void print_binary (FILE *out) {
	int mask = 1;
	for (int i = first_addr(); i <= last_addr(); i+=MEM_OFFSET) {
		int val = load_word(i);
		
		for (int j = 31; j >= 0; j--) {
			if (val & (mask<<j)) {
				fprintf(out,"1");
			} else fprintf(out,"0");
		}
		fprintf (out,"\n");
	}
}


void print_text_segment(FILE *out, int hexText){
	const char* fmt_text = hexText ? "0x%08x" : "%10u";
	int first_address = first_text_seg_addr(), 
		last_address = last_text_seg_addr();
	int i,j;

	fprintf (out, "\n****\t\t\tTEXT SEGMENT (LOWER TO HIGHER)\t\t\t****\n\n");
	
	while (load_word(last_address) == 0) 
		last_address -= MEM_OFFSET;

	fprintf (out, "%-7s: %6s\n", "ADDRESS", "CODE");
	for (i = first_address; i <= last_address; i+=MEM_OFFSET) {
		fprintf (out, "0x%04x : ", i);
		fprintf (out, fmt_text, load_word(i));
		fprintf (out, "\n");
	}
}

void print_data_segment(FILE *out, int hexData){
	const char* fmt_data = hexData ? "0x%08x" : "%10d";
	int first_data_address = first_data_seg_addr(),
		last_data_address = last_data_seg_addr();
	int i,j;

	

	while (load_word(last_data_address) == 0 
			&& load_word(last_data_address-MEM_OFFSET) == 0
			&& load_word(last_data_address-MEM_OFFSET*2) == 0
			&& load_word(last_data_address-MEM_OFFSET*3) == 0
			&& load_word(last_data_address-MEM_OFFSET*4) == 0
			&& load_word(last_data_address-MEM_OFFSET*5) == 0
			&& load_word(last_data_address-MEM_OFFSET*6) == 0
			&& load_word(last_data_address-MEM_OFFSET*7) == 0
			&& last_data_address > first_data_address)
		last_data_address -= MEM_OFFSET*8;

		

	fprintf (out,"\n****\t\t\tDATA SEGMENT (LOWER TO HIGHER)\t\t\t****\n\n");

	fprintf (out, "%-7s: ", "ADDRESS");
	fprintf (out, "%12s %12s %12s %12s ", "Value (+0)", "Value (+4)", "Value (+8)", "Value (+c)");
	fprintf (out, " %12s %12s %12s %12s\n", "Value (+10)", "Value (+14)", "Value (+18)", "Value (+1c)");

	for (i = first_data_address; i <= last_data_address; i+= MEM_OFFSET*8){
		fprintf (out, "0x%04x :", i);
		for (j = i; j <= i+0x1c; j+=MEM_OFFSET) {
			fprintf (out, "   ");
			fprintf (out, fmt_data, load_word(j));
		}
		fprintf (out, "\n");
	}
}

void print_stack_segment(FILE *out, int hexStack) {
	const char* fmt_stack = hexStack ? "0x%08x" : "%10d";
	int first_stack_address = 0x10000-MEM_OFFSET*8,
		last_stack_address = last_data_seg_addr()+MEM_OFFSET;
	int i,j;

	while (load_word(last_stack_address) == 0 
			&& load_word(last_stack_address+MEM_OFFSET) == 0
			&& load_word(last_stack_address+MEM_OFFSET*2) == 0
			&& load_word(last_stack_address+MEM_OFFSET*3) == 0
			&& load_word(last_stack_address+MEM_OFFSET*4) == 0
			&& load_word(last_stack_address+MEM_OFFSET*5) == 0
			&& load_word(last_stack_address+MEM_OFFSET*6) == 0
			&& load_word(last_stack_address+MEM_OFFSET*7) == 0
			&& last_stack_address <= first_stack_address)
		last_stack_address += MEM_OFFSET*8;

	fprintf (out, "\n****\t\t\tSTACK SEGMENT (HIGHER TO LOWER)\t\t\t****\n\n");

	fprintf (out, "%-7s: ", "ADDRESS");
	fprintf (out, "%12s %12s %12s %12s ", "Value (+0)", "Value (+4)", "Value (+8)", "Value (+c)");
	fprintf (out, " %12s %12s %12s %12s\n", "Value (+10)", "Value (+14)", "Value (+18)", "Value (+1c)");

	
	if (first_stack_address+MEM_OFFSET*8 != last_stack_address){
		for (i = first_stack_address; i >= last_stack_address; i-= MEM_OFFSET*8) {
			fprintf (out, "0x%04x :", i);
			for (j = i; j <= i+0x1c; j+=MEM_OFFSET) {
				fprintf (out, "   ");
				fprintf (out, fmt_stack, load_word(j));
			}
			fprintf (out, "\n");
		}

		

		last_stack_address -= MEM_OFFSET*8;

		if (last_stack_address != last_data_seg_addr()+MEM_OFFSET)
			fprintf (out, "[0x%04x ... 0x%04x] : 0\n", 
					last_data_seg_addr()+MEM_OFFSET, last_stack_address+MEM_OFFSET*7);

	} else {
		fprintf (out, "\n---- EMPTY STACK ----\n\n");
	}
}