#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <instruction_fetcher.h>
#include <memory_controller.h>
#include <registers_file.h>
#include <instruction_execution.h>
#include <instruction_decoder.h>


char loadedProgram[50] = "";

void setup() {
	clearRegisters();
	preAllocateRegisters();
	allocate_memory();
	clear_memory();
	initialize_executor();
	clear_pc();
}

void file_printer(){
	const char* memory_filename = "memory.out";
	const char* registerfile_filename = "registers_file.out";
	const char* regbinary_filename = "registers_file.bin";
	const char* membinary_filename = "memory.bin";

	FILE *mem_file = fopen(memory_filename,"w+");
	FILE *reg_file = fopen(registerfile_filename, "w+");
	FILE *reg_file_bin = fopen(regbinary_filename, "w+");
	FILE *mem_file_bin = fopen(membinary_filename, "w+");

	printRegisters(reg_file, 0);
	print_memory(mem_file,1,0,0);
	print_binary(mem_file_bin);
	printRegistersBinary(reg_file_bin);

	fclose(reg_file);
	fclose(mem_file);
	fclose(mem_file_bin);
	fclose(reg_file_bin);
}

void execution_cycle() {
	int instruction;
	int* args;
	while (read_pc() <= last_text_seg_addr()) {
		instruction = fetch_instruction();		
		increment_pc();
		args = decode_instruction(instruction);
		execute_instruction(args);	
		free(args);	
	}
	//printRegisters(stdout, 0);
	//print_memory(stdout, 1, 0);
	printf ("\n\nExecução finalizada! \n");
	file_printer();
	printf ("Arquivos memory.out e registers_file.out gerados. \n");
}


void loadProgram(){
	char path[50];

	setup();
	printf ("Digite caminho do programa: ");
	scanf ("%s",path);
	fill_memory(path);
	strcpy(loadedProgram, path);
}

void menu() {
	int op = 0;
	while(1){
		printf ("\n\n----- Simulador -----\n\n");
		printf ("Programa Carregado: %s\n\n",loadedProgram);
		printf ("1 - Carregar Programa\n");
		printf ("2 - Executar Programa Carregado\n");
		printf ("3 - Imprimir Registradores\n");
		printf ("4 - Imprimir Segmento de Texto\n");
		printf ("5 - Imprimir Segmento de Dados\n");
		printf ("6 - Imprimir Segmento da Pilha\n\n");

		scanf ("%d", &op);

		switch (op) {
			case 1: loadProgram(); break;
			case 2: execution_cycle(); break;
			case 3: printRegisters(stdout, 0); break;
			case 4: print_text_segment(stdout,1); break;
			case 5: print_data_segment(stdout, 0); break;
			case 6: print_stack_segment(stdout, 0); break;
		}
	}
}


int main(int argc, char *argv[]){	
	
	setup();
	
	//fill_memory(argv[1]);

	//execution_cycle();

	//file_printer();
	menu();
	return 0;
}

