Problema 1 - Simulador

Linguagem de Progamação
--------
* Python 2.7 (incompleto)
* C (completo)

Compilação
---------
```
cd c
make
```

Execução
--------

```
cd c
./main
```

Carregamento de Programa
-----------

* Para carregar um programa no simulador, selecione a opção 1 do menu e especifique o caminho do arquivo .mc ou .hmc relativo à pasta do main.